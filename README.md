## Setting Up:

**Step 1:** Download the repostory using the command:  
```
 Git Clone "https://gitlab.com/ced_b3_projects/ced-b3-g03.git" 
 ```
**Step 2:** Install the dependecies using the command: 
```
 npm Install  
 ```
**Step 3:** Use the following command to compile smart contract:  
```
 truffle compile  
 ```
**Step 4:** Use one of the following commands to connect to the devchain,private or public chain:  

**Devchain:**

1: Run the chain using following command
```
geth --datadir dev --networkid "4002" --rpc --rpcport 8545 --ipcpath "~/.ethereum/geth.ipc" --rpccorsdomain "*" --rpcapi "db,personal, eth, web3, net" --dev
```
2: In new terminal find coin base address and Copy to app.js
```
geth attach
> eth.accounts
```
3: Copy coin base address to app.js.Create 4 more accounts and pass ether to them using following comments
```
personal.newAccount("")//4 times
eth.sendTransaction({from:eth.accounts[0],to:eth.accounts[1],value:150000000000000000000})//pass 150 ether to each account
```
4: Cut the chain and rerun the chain using following comment to unlock the new accounts
```
geth --datadir dev --networkid "4002" --rpc --rpcport 8545 --ipcpath "~/.ethereum/geth.ipc" --rpccorsdomain "*" --rpcapi "db,personal, eth, web3, net" --dev --unlock 1,2,3,4 --allow-insecure-unlock
```
*Password: Enter Key*

**Private chain:** 
```
cd geth

geth --identity "miner" --networkid 4002 --datadir data --nodiscover --mine --rpc --rpcport "8545" --port "8191" --unlock 0,1,2,3 --password password.txt --ipcpath "~/.ethereum/geth.ipc" --rpccorsdomain "*" --rpcapi "db,eth,net,web3,personal" --allow-insecure-unlock 
```

start mining using the following command in new terminal, before doing any transaction:  
```
  geth attach 

  miner.start()  
```
*Password: Enter Key*

The nodedata folder is in the project root  

**Ropsten:**  

1: Run the chain using following command
```
 geth --testnet --syncmode "light" --datadir "rop" --rpc --rpcapi "db,eth,net,web3,personal" --rpcport "8545"  --rpccorsdomain "*" --allow-insecure-unlock
```
2: use java script console and connect to running chain.
```
    geth attach http://127.0.0.1:8545
```
3: Wait for the node to synch fully (will take some time). use the below command to know the status
```
    eth.syncing
```
    false - incase no synching happening (same for synching finished also), otherwise will show the status

4: Create a new account using the following command
```
    personal.newAccount("")
```
     The password is an enter

    create 3 more accounts

3: Transfer ether to each account using metamask account

4:Cut and ReRun the chain using following command
```
geth --testnet --syncmode "light" --datadir "rop" --rpc --rpcapi "db,eth,net,web3,personal" --rpcport "8545"  --rpccorsdomain "*" -unlock 0,1,2,3 --allow-insecure-unlock
 ```
**Step 5:** Use the following command to deploy the smart contract to the connected chain: 
```
 truffle migrate  
 ```
**Step 6:** Run the dapp using the command  
```
 npm start  
```
## Execution Flow:
To view balance in accounts
```
web3.fromWei(eth.getBalance(eth.accounts[1]), "ether") 
```

**Step 7:** Go http://localhost:3000/,Click register button,Select an address and enter a village name  to add an admin to that village.  Provide the  details below and generate an id
```
ex:  
  state = kerala;
  district = kottayam;
  village=kanakkary;
  surveyNo = 1;
``` 
**Step 8:** Using the admin address of a village register the property in that village by entering the below details
```
  ex:  
  admin adress = select;
  land owner address = select;
  state = kerala;
  district = kottayam;
  village=kanakkary;
  surveyNo = 1;
  value = 4;
  Land id = above created id;
``` 
**Step 9:** Select admin address and enter village and land id to view details
```
  ex:  
  admin adress = select;
  village=kanakkary;
  Land id = above created id;
```
**Step 10:** Click users on top. Select an account address, click view assets to view lands registered in that account.Copy a land id to view its details and make avaiable to sale. 

**Step 11:** Click users on top.To view a property to buy it select an address other than admin or owner and enter the land id and click submit button.Here select your address ,add a proposed value and click Request to land button.

**Step 12:** Click users on top. Select the address on which land was requested ,enter land id and click view details.Here click accept button to accept or reject button to reject the request.

**Step 12:** Click users on top. To view an accepeted property to buy, select requested address and enter the land id and click submit button,Here select requested address and click buy button

**Step 12:** You can see the property moved to requester address by select address in view assets

END
