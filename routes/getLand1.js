var express = require('express');
var router = express.Router();

// view the details of property by current owner
router.get('/', function (req, res, next) {
    var accountAddress = req.query.addressList;
    console.log(accountAddress);
    MyContract.methods.viewDetails(req.query.lid)
        .call({ from: accountAddress })
        .then((result) => {
            console.log(result);
            res.render("getLand1", { title: 'Land Registration',result: result });
        })
});
module.exports = router;