var express = require("express");
var router = express.Router();

//view the assets to the owner 

router.post('/', function (req, res, next) {

    data = req.body;
    console.log(data);
    var accountAddress = req.body.accountAddress;

    let result = MyContract.methods.viewAssets()
        .call({ from: accountAddress })
        .then((txn) => {
            console.log(txn);
            res.json({ txn: txn });

        }).catch(err => {
            res.json({ error: err });
        })
})
module.exports = router;