var express = require('express');
var router = express.Router();

// Router to generate propertyId

router.post('/', async function (req, res, next) {
    data = req.body;
    console.log(data);
    let result = MyContract.methods.propertyIdGeneration(data.state, data.district,data.village,data.surveyNo)
        .call({ from: accountAddress })
        .then((txn) => {
            console.log(txn);
            
            res.json({  txn : txn  });

        }).catch(err=>{
            res.json({ error : err });
        })
});
module.exports = router;