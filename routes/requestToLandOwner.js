var express = require('express');
var router = express.Router();

// Router sending request to land owner

router.post('/', function (req, res, next) {
    var accountAddress = req.body.AddressList;
    data = req.body;
    console.log(data);
    const land_id = parseInt(data.land_id);
    MyContract.methods.requestToLandOwner(land_id, data.value)
        .send({ from: accountAddress })
        .then((txn) => {
            res.send(txn);
        })
});

module.exports = router;