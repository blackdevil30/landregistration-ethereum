var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Land Registration'})
});
// module.exports = router;

//buyers page
router.get('/buyer',function (req,res,next){
  res.render('buyer',{ title: 'Land Registration'})
  });

// get admin page

// module.exports = router;
router.get('/admin', function (req, res, next) {
 
  web3.eth.getAccounts().then((listAcc)=>{
    console.log(listAcc);
  res.render('admin', { title: 'Land Registration',data1: listAcc})
});
});
// module.exports = router;

//get user page

router.get('/users', function (req, res, next) {

  web3.eth.getAccounts().then((listAcc)=>{
    console.log(listAcc);
    res.render('users', { title: 'Land Registration', data: listAcc})
    });
  });

module.exports = router;


