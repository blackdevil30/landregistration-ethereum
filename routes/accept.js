var express = require('express');
var router = express.Router();

// Accept the Land request router

router.post('/', function (req, res, next) {
    var accountAddress = req.body.AddressList;
    console.log(accountAddress);
    data = req.body;
    console.log(data);
    const status = parseInt(data.status);
    MyContract.methods.processRequest(data.property, status)
        .send({ from: accountAddress })
        .then((txn) => {
            res.send(txn);
        })
});
module.exports = router;