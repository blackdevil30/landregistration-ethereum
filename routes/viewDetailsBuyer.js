var express = require('express');
var router = express.Router();

//Router to view details for buyer
router.get('/', function (req, res, next) {
    web3.eth.getAccounts().then((listAcc)=>{
        MyContract.methods.viewDetailsBuyer(req.query.id)
        .call({ from: accountAddress })
        .then((result) => {
            console.log(result);
            res.render("viewDetailsBuyer", { title: 'Land Registration',data: listAcc,result: result });
        })
    });
});
module.exports = router;