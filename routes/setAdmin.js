var express = require('express');
var router = express.Router();

// Router to assign Admin to a village//
router.post('/', async function (req, res, next) {
    data = req.body;
    console.log(data);
    var accountAddress = req.body.addressList;
    MyContract.methods.assignadmin(data.admin, data.village)
        .send({ from: accountAddress })
        .then((txn) => {
            res.send(txn);
        }).catch(err=>{
            res.json({ error : err });
        }) 
        
});
module.exports = router;