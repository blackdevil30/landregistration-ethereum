var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var setLandRouter = require("./routes/setLand");
var setAdminRouter = require("./routes/setAdmin");
var getLandRouter = require("./routes/getLand");
var getLand1Router = require("./routes/getLand1");
var propertyIdGenerationRouter = require("./routes/propertyIdGeneration");
var assetRouter = require("./routes/assets");
var viewDetailsBuyerRouter = require("./routes/viewDetailsBuyer");
var requestToLandOwnerRouter = require("./routes/requestToLandOwner");
var makeAvailRouter = require("./routes/makeAvail");
var buyPropertyRouter = require("./routes/buyProperty");
var acceptRouter = require("./routes/accept");

//-------------------WEB3 Integration starts-----------------------

var Web3 = require('web3');

var ContractJSON = require(path.join(__dirname, 'build/contracts/landRegistration.json'))//path to json file in other folder

web3 = new Web3("http://localhost:8545"); //connect web3 to block chain

accountAddress = "0xc742eb31802556ac0ed4e390abaa1ff8133dbbc1";

//contractAddress = ContractJSON.networks['4002'].address;//global access no var//get contract address from 4002

contractAddress = ContractJSON.networks['3'].address;//get contract address from Ropston test net

const abi = ContractJSON.abi;

MyContract = new web3.eth.Contract(abi, contractAddress);//access contract details from contract

//-------------------WEB3 Integration Stops------------------------

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/setLand', setLandRouter);
app.use('/setAdmin', setAdminRouter);

app.use('/getLand', getLandRouter);
app.use('/getLand1', getLand1Router);
app.use('/propertyIdGeneration', propertyIdGenerationRouter);
app.use('/assets', assetRouter);
app.use('/viewDetailsBuyer', viewDetailsBuyerRouter);
app.use('/requestToLandOwner', requestToLandOwnerRouter);
app.use('/makeAvail',makeAvailRouter);
app.use('/buyProperty', buyPropertyRouter);
app.use('/accept',acceptRouter);

// catch 404 and forward to error handler

app.use(function (req, res, next) {
  next(createError(404));
});

// error handler

app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
