pragma solidity ^0.5.0;

contract landRegistration{

    address pOwner;// pOwner is the property pOwner
    
/*struct details: the structure to get the property details
state:the the state in which property is located
district:
village:
surveyNo:
value:the market Value of the property
requester:The one who make request for the property
currentOwner:the current owner of the property
landStatus:The status of the property
Available:to check whether the property Available for sale or not
proposedValue:Buyer proposed value of the property 
*/
    
  struct details
     {
    string state;
    string district;
    string village;
    uint256 surveyNo;
    uint value;
    address requester;
    address payable currentOwner;
    lStatus landStatus;
    bool Available;
    uint proposedValue;
    }
    
    //to request status
    enum lStatus{Default,Pending,Reject,Approved}
    
    //stores the asset list of a person
    struct profiles{
        uint[] assetList;
    }
    
    //mapping
    mapping(uint => details)land;
    mapping(string => address)admin;
    mapping(address => profiles)profile;
    
    //events
    event RegistrationEvent(string message,uint256 value,address currentOwner);
    event newOwnerEvent(string message,uint256 value,address currentOwner);    
    event OwnerRemovedEvent(string message,uint256 value);
   constructor() public{
       pOwner=msg.sender;
   }
   
    //to avil accessibility for owner only
    
    modifier onlyOwner(){
        require(msg.sender==pOwner);
        _;
    }
        
    //function to assign admin to a village
    
    function assignadmin(address _admin,string memory _village) public onlyOwner{
        admin[_village]=_admin;
    }
    
    //functon to generate propertyId
            
    function propertyIdGeneration(string memory _state,string memory _district,string memory _village,uint _surveyNo)public pure returns(uint){
        return uint(keccak256(abi.encodePacked(_state,_district,_village,_surveyNo)))%10000000000000;
        
    }
    
    //registration of land
    
    function Registration(string memory _state,string memory _district,string memory _village,uint256 _surveyNo,uint _value,address payable _currentOwner,uint id) public returns (bool){
    require(admin[_village] == msg.sender || pOwner == msg.sender,"Only Registration is possible by admin");
    land[id].state=_state;
    land[id].district=_district;
    land[id].village=_village;
    land[id].surveyNo=_surveyNo;
    land[id].value=_value;
    land[id].currentOwner=_currentOwner;
    profile[_currentOwner].assetList.push(id);
    emit RegistrationEvent('new Land registred',id,_currentOwner);
    return true;
        }

    //function to view details to admin

    function viewDetails1(uint id,string memory _village2) public view returns(string memory _state,string memory _district,string memory _village1,uint256 _surveyNo,uint _value,address _currentOwner,uint _id){
        require(admin[_village2] == msg.sender || pOwner == msg.sender,"Only view    is possible by admin and owner");
        // require(land[id].village == village2);
        return(land[id].state,land[id].district,land[id].village,land[id].surveyNo,land[id].value,land[id].currentOwner,id);
    }

     //will show assets of the function caller 

    function viewAssets()public view returns(uint[] memory){
        return (profile[msg.sender].assetList);
    }

    //function to view the details of property by current owner
    
    function viewDetails(uint id) public view returns(uint256 _surveyNo,bool _Available,address _currentOwner,address _requester,uint _proposedValue,uint _id,lStatus _landStatus){
        require(land[id].currentOwner == msg.sender,"Only view to Owner of land");
        return(land[id].surveyNo,land[id].Available,land[id].currentOwner,land[id].requester,land[id].proposedValue,id,land[id].landStatus);
    }      
       
    //availing land for sale.

    function makeAvailableToSale(uint property)public{
        require(land[property].currentOwner == msg.sender,"Only made available by owner");
        land[property].Available=true;
    } 
    
    //function to view details for buyer
    
    function viewDetailsBuyer(uint id)public view returns(address,uint,bool,address,uint,lStatus,uint){
        return(land[id].currentOwner,land[id].value,land[id].Available,land[id].requester,land[id].proposedValue,land[id].landStatus,id);
    }
            
    //sending request to land owner
    
    function requestToLandOwner(uint id,uint _value)public payable{
        require(msg.sender != land[id].currentOwner);
        require(land[id].Available,"request only available available property");
        land[id].requester=msg.sender;
        land[id].proposedValue=_value;
        land[id].Available=false;
        land[id].landStatus = lStatus.Pending; //changes the status to pending.
        }
    
    //viewing request for the lands

    function viewRequest(uint property)public view returns(address,uint){
        return(land[property].requester,land[property].proposedValue);
    }
    
    //processing request for the land by accepting or rejecting

    function processRequest(uint property,lStatus status)public {
        require(land[property].currentOwner == msg.sender,"only currentOwner can process Request");
        land[property].landStatus=status;
        if(status == lStatus.Reject){
            land[property].requester = address(0);
            land[property].proposedValue=0;
            land[property].landStatus = lStatus.Default;
        }
    }
        
    //buying the approved property

    function buyProperty(uint property)public payable{
       
        require(land[property].landStatus == lStatus.Approved,"only buy Approved property");
        require( msg.sender == land[property].requester,"only Approved requester can buy");
        require(land[property].proposedValue >= land[property].value,"Land value must be equal or above proposed value");
        land[property].currentOwner.transfer((address(this).balance));
        removeOwnership(land[property].currentOwner,property);
        emit OwnerRemovedEvent('previous Owner Removed from',property);
        land[property].currentOwner=msg.sender;
        land[property].Available=false;
        land[property].requester = address(0);
        land[property].proposedValue=0;
        land[property].landStatus = lStatus.Default;
        profile[msg.sender].assetList.push(property); //adds the property to the asset list of the new owner.
        emit newOwnerEvent('New owner added to ',property,msg.sender);
    }

    //removing the ownership of seller for the land. and it is called by the buyProperty function

    function removeOwnership(address previousOwner,uint id)public{
        uint index = findId(id,previousOwner);
        profile[previousOwner].assetList[index]=profile[previousOwner].assetList[profile[previousOwner].assetList.length-1];
        delete profile[previousOwner].assetList[profile[previousOwner].assetList.length-1];
        profile[previousOwner].assetList.length--;
        
    }

    //for finding the index of a perticular id

    function findId(uint id,address user)public view returns(uint){
        uint i;
        for(i=0;i<profile[user].assetList.length;i++){
            if(profile[user].assetList[i] == id)
                return i;
        }
        return i;
    }
    
}